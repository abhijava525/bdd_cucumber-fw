Feature: Trigger the Patch API and Validate Response Body and Response Body Parameters

@Patch_API_TestCases
Scenario: Trigger the Patch API Request with valid String Request Body Parameters
          Given Name and Job in Request Body for Patch API
          When Send the Request with Payload to the Endpoint for Patch API
          Then Validate Status Code of Patch API
          And Validate Response Body Parameters of Patch API
          