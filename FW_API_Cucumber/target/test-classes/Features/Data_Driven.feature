Feature: Trigger the Post API on the Basis of Input Data

@DataDriven
Scenario Outline: Trigger the Post API Request with Valid Request Parameters

 Given "<Name>" and "<Job>" in Request Body 
          When Send the Request with Payload to the Endpoint 
          Then Validate Status Code
          And Validate Response Body Parameters
          
 Examples:
           |Name |Job |
           |Sandip|Lead|
            |Kunal|SQA|