Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Kunal",
    "job": "SQA"
}

Response body is : 
{"name":"Kunal","job":"SQA","id":"703","createdAt":"2024-03-18T14:58:52.735Z"}Response Header is : 
Mon, 18 Mar 2024 14:58:52 GMT

