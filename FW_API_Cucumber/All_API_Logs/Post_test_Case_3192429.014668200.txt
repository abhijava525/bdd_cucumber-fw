Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Kunal",
    "job": "SQA"
}

Response body is : 
{"name":"Kunal","job":"SQA","id":"147","createdAt":"2024-03-19T13:54:29.101Z"}Response Header is : 
Tue, 19 Mar 2024 13:54:29 GMT

