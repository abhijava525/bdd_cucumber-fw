Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response body is : 
{"name":"morpheus","job":"leader","id":"194","createdAt":"2024-03-19T14:32:23.170Z"}Response Header is : 
Tue, 19 Mar 2024 14:32:23 GMT

