package Common_Methods;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {

	public static Response Post_trigger(String HeaderName, String HeaderValue, String reqbody, String Endpoint) {

		RequestSpecification requestSpec = RestAssured.given();

		requestSpec.header(HeaderName, HeaderValue);

		requestSpec.body(reqbody);

		Response response = requestSpec.post(Endpoint);

		return response;

	}
	
	public static Response Trigger_Patch(String HeaderName, String HeaderValue, String Req_Body, String Endpoint) {

		RequestSpecification reqSpec = RestAssured.given();

		reqSpec.header(HeaderName, HeaderValue);
		reqSpec.body(Req_Body);

		Response response = reqSpec.patch(Endpoint);

		return response;

	}

	public static Response Trigger_Put(String HeaderName, String HeaderValue, String Req_Body, String Endpoint) {

		RequestSpecification reqSpec = RestAssured.given();

		reqSpec.header(HeaderName, HeaderValue);
		reqSpec.body(Req_Body);

		Response response = reqSpec.patch(Endpoint);

		return response;

	}
	
	public static Response Trigger_Get(String HeaderName, String HeaderValue, String Endpoint) {
		
		RequestSpecification reqSpec = RestAssured.given();
		
		reqSpec.header(HeaderName, HeaderValue);
		
		Response response = reqSpec.get(Endpoint);
		
		return response;
	}
	
	public static Response Trigger_Delete(String HeaderName, String HeaderValue, String Endpoint) {
		
		RequestSpecification reqSpec = RestAssured.given();
		
		reqSpec.header(HeaderName, HeaderValue);
		
		Response response = reqSpec.delete(Endpoint);
		
		return response;
		
	}

}