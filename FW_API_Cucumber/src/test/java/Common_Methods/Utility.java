
package Common_Methods;

import java.io.File;
import java.io.FileInputStream;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.DeferredSXSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {
	public static ArrayList<String> ReadExcelData1(String SheetName, String TestCase) throws IOException {

		ArrayList<String> arrayData = new ArrayList<String>();

	FileInputStream fis = new FileInputStream("DataFiles\\Input_Data.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);

		int count = wb.getNumberOfSheets();

		String sheetfound = "False";

		for (int i = 0; i < count; i++) {
			if (wb.getSheetName(i).equals(SheetName)) {

				sheetfound = "True";
				XSSFSheet dsheet = wb.getSheetAt(i);

				Iterator<Row> rows = dsheet.iterator();

				String testcasefound = "false";

				while (rows.hasNext()) {
					Row datarows = rows.next();

					String tcname = datarows.getCell(0).getStringCellValue();

					if (tcname.equals(TestCase)) {
						testcasefound = "true";

						Iterator<Cell> cellvalues = datarows.cellIterator();

						while (cellvalues.hasNext()) {
							Cell cell = cellvalues.next();
							String testdata = "";

							// Handle multiple data type method number 1
							/*
							 * try { testdata = Cell.getStringCellValue(); } catch(IllegalStateException e)
							 * { double inttestData = Cell.getNumericCellValue(); testdata =
							 * String.valueOf(inttestData); }
							 */

							// Handle multiple data type method number 2

							CellType datatype = cell.getCellType();
							if (datatype.toString().equals("STRING")) {
								testdata = cell.getStringCellValue();
							} else if (datatype.toString().equals("NUMERIC")) {
								double inttestData = cell.getNumericCellValue();
								testdata = String.valueOf(inttestData);
							}

							// System.out.println(testdata);
							// Step 4.3 : Add data in array list
							arrayData.add(testdata);
						}

						break;
					}

				}
				if (testcasefound.equals("false")) {
					System.out.println(TestCase + "   Test Case not Found in Sheet" + wb.getSheetName(i));
				}
				break;

			}

		}

		if (sheetfound.equals("False")) {
			System.out.println(SheetName + "    not Found in Data_Sheet.xlsx");

		}

		wb.close();
		return arrayData;
	}
	  
	

                 // 2. method = evidence file creator
	public static void evidenceFileCreator(String Filename, File FileLocation, String endpoint, String RequestBody,
			String ResHeader, String ResponseBody) throws IOException {
		
		// Step 1 :  Open and Create  the file
		File newTextFile = new File(FileLocation + "\\" + Filename + ".txt");
		
		//step 1.1 = make sure that filename and file location is created or not,4 name and location is successfully
		System.out.println("File create with name: " + newTextFile.getName());

		
		
		// Step 2 : Write data
				FileWriter writedata = new FileWriter(newTextFile);
				writedata.write("Endpoint is :\n" + endpoint + "\n\n");
				writedata.write("Request body is :\n" + RequestBody + "\n\n");
				writedata.write("Response body is : \n" + ResponseBody);
				writedata.write("Response Header is : \n" +ResHeader+ "\n\n");
				// Step 3 : Save and close
				writedata.close();
	}
	
	         //3 . Createlogdirectory 
	public static File CreateLogDirectory(String dirName) {
		//step 1: Fetch the java project name and location
		
		String ProjrctDir = System.getProperty("user.dir");
		// Step 1.1 = make sure that project name and location is successfully fetch or not
		System.out.println("Current Project Directory Is"+ProjrctDir);
		
		//Step2 :verify whether the directory stored variable name = dirName is available in ProjrctDir
		File directory =new File(ProjrctDir+"\\"+dirName);
		
		if (directory.exists()) {
		System.out.println(directory+", already exists");
	}
		
		else {
			System.out.println(directory+", does not exists, hence creating it");
			// directory.mkdir(); = to create new folder or directory
			directory.mkdir();
			System.out.println(directory+ ", Created");
		}
		return directory;
	}
	public static String testLogName (String Name) {
		LocalTime currentTime = LocalTime.now();
		String currentstringTime=currentTime.toString().replaceAll(":", "");
		String TestLogName = Name+currentstringTime;
		return TestLogName;
	}



}

