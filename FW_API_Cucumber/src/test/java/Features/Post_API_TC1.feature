Feature: Trigger the Post API and Validate Response Body and Response Body Parameters

@Post_API_TestCases
Scenario: Trigger the API Request with valid String Request Body Parameters
          Given Name and Job in Request Body 
          When Send the Request with Payload to the Endpoint 
          Then Validate Status Code
          And Validate Response Body Parameters
          