Feature: Trigger the GET API and Validate Response code

@GET_API_TestCases
Scenario: Trigger the API Request with valid Endpoint For GET
          Given Valid Required Credentials are provided For Get
          When Request Sent For Get Resource 
          Then Validate Status Code For Get
          