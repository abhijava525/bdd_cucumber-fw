package Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.Utility;

public class RequestBody extends Environment {

	public static String req_tc1() throws IOException {
		// To call excel sheet in requestbody
		ArrayList<String> Data = Utility.ReadExcelData1("Post_Api", "Post_TC2");

		// to use the data of excelsheet of testcase
		String Key_name = Data.get(1);
		String Value_name = Data.get(2);
		String Key_job = Data.get(3);
		String Value_job = Data.get(4);

		System.out.println(Data);
		String req_body = "{\r\n" + "    \"" + Key_name + "\": \"" + Value_name + "\",\r\n" + "    \"" + Key_job
				+ "\": \"" + Value_job + "\"\r\n" + "}";
		return req_body;
	}

	public static String Body_Patch_Update() {

		String body_patch_update = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"
				+ "}";

		return body_patch_update;
	}

	public static String Body_Put_Update() {

		String body_put_update = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"
				+ "}";

		return body_put_update;
	}

}