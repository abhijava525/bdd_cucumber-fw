package Repository;

public class Environment {

	public static String Hostname() {
		String hostname = "https://reqres.in/";
		return hostname;
	}

	public static String HeaderName() {

		String headername = "Content-Type";
		return headername;
	}

	public static String HeaderValue() {

		String headervalue = "application/json";
		return headervalue;
	}

	public static String Resource() {

		String endpoint = "api/users";

		return endpoint;
	}

	public static String Res_Patch_Update() {

		String res_patch_update = "api/users/2";

		return res_patch_update;

	}

	public static String Res_Put_Update() {

		String res_put_update = "api/users/2";

		return res_put_update;

	}

	public static String Res_Get_ListUser() {

		String res_get_listuser = "api/users?page=2";

		return res_get_listuser;

	}
	
	public static String Source_Delete() {
		String source_put_update = "api/users/2";
		return source_put_update;
	}
	
	

}
