package stepDefinition;

import java.io.File;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import junit.framework.Assert;

public class GetstepDefinition {
	Response response;
	String Endpoint;
	 File dir_name;
	 int Statuscode;
	 
	@Given("Valid Required Credentials are provided For Get")
	public void valid_required_credentials_are_provided_for_get() {
		dir_name = Utility.CreateLogDirectory("All_API_Logs");
		Endpoint = RequestBody.Hostname() + RequestBody.Res_Get_ListUser();
	    response = API_Trigger.Trigger_Get(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);
	}
	@When("Request Sent For Get Resource")
	public void request_sent_for_get_resource() {
		Statuscode = response.statusCode();
	    
	}
	@Then("Validate Status Code For Get")
	public void validate_status_code_for_get() {
		   Assert.assertEquals(Statuscode, 200);
	  
	}


}
