package stepDefinition;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PoststepDefinition {
	Response response;
	String Endpoint;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;
	File dir_name;
	
	@Before
	public void setup()
	{
		
		System.out.println("Executing Scenario");
	}
	
	
	@After
	public void tearDown()
	{
		
		System.out.println("Scenario Completed with validations");
	}
	
	@Given("{string} and {string} in Request Body")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
		dir_name = Utility.CreateLogDirectory("All_API_Logs");
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Post_test_Case_3"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

	}


	@Given("Name and Job in Request Body")
	public void name_and_job_in_request_body() throws IOException {

		dir_name = Utility.CreateLogDirectory("All_API_Logs");
		requestBody = RequestBody.req_tc1();
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Post_test_Case_3"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Request with Payload to the Endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {
		statuscode = response.statusCode();
		System.out.println(statuscode);
		ResponseBody res_body = response.getBody();
		System.out.println(res_body.asString());
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Status Code")
	public void validate_status_code() {
		Assert.assertEquals(response.statusCode(), 201);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Response Body Parameters")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
		//throw new io.cucumber.java.PendingException();
	}

}
