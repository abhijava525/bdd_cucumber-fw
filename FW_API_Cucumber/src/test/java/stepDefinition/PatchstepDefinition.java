package stepDefinition;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PatchstepDefinition {
	Response response;
	String Endpoint;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_updatedAt;
	String requestBody;
	
	File dir_name;
	
	@Given("Name and Job in Request Body for Patch API")
	public void name_and_job_in_request_body_for_patch_api() throws IOException {
		dir_name = Utility.CreateLogDirectory("All_API_Logs");
		requestBody = RequestBody.Body_Patch_Update();
		Endpoint = RequestBody.Hostname() + RequestBody.Res_Patch_Update();
		response = API_Trigger.Trigger_Patch(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Post_test_Case_3"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		
	   
	    
	}
	@When("Send the Request with Payload to the Endpoint for Patch API")
	public void send_the_request_with_payload_to_the_endpoint_for_patch_api() {
		statuscode = response.statusCode();
		System.out.println(statuscode);
		ResponseBody res_body = response.getBody();
		System.out.println(res_body.asString());
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_updatedAt = res_body.jsonPath().getString("updatedAt");
		System.out.println(res_updatedAt);
		res_updatedAt = res_updatedAt.substring(0, 11);
	    
	}
	@Then("Validate Status Code of Patch API")
	public void validate_status_code_of_patch_api() {
	   Assert.assertEquals(statuscode, 200);
	    
	}
	@Then("Validate Response Body Parameters of Patch API")
	public void validate_response_body_parameters_of_patch_api() {
		
			JsonPath jsp_req = new JsonPath(requestBody);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");
			LocalDateTime currentdate = LocalDateTime.now();
			String expecteddate = currentdate.toString().substring(0, 11);

			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
			Assert.assertEquals(res_updatedAt, expecteddate);
			
		}

	
	    
	}




