package stepDefinition;

import java.io.File;
import java.io.IOException;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import junit.framework.Assert;

public class DeletestepDefinition {
	Response response; 
	int Statuscode;
	@Given("Valid Required Credentials are provided")
	public void valid_required_credentials_are_provided() throws IOException {
		File dir_name = Utility.CreateLogDirectory("All_API_Logs");

		String Endpoint = RequestBody.Hostname() + RequestBody.Source_Delete();
		response = API_Trigger.Trigger_Delete(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				 Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Delete_test_Case_3"), dir_name, Endpoint, RequestBody.Body_Put_Update(),
				response.getHeader("Date"), response.getBody().asString());
	  
	}
	@When("Request Send to Delete Resource")
	public void request_send_to_delete_resource() {
		Statuscode = response.statusCode();
	}
	@Then("Validate Status Code For Delete")
	public void validate_status_code_for_delete() {
	   Assert.assertEquals(Statuscode, 204);
	}

}
